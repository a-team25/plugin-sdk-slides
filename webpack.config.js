const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('dist/')
    //.setPublicPath('/dist')
    .setPublicPath(Encore.isProduction() ? 'https://a-team25.gitlab.io/plugin-sdk-slides/' : '/dist')
    .setManifestKeyPrefix('')

    .addEntry('frontend', './src/js/app.js')
    //.addStyleEntry('style', './src/scss/styles.scss')

    // .copyFiles({
    //     from: './node_modules/font-awesome',
    //     to: './vendor/font-awesome/[path][name].[ext]'
    // })

    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps()
    //.enableVersioning()

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .enableSassLoader()
    .enablePostCssLoader()
;

module.exports = Encore.getWebpackConfig();
