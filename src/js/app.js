import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Highlight from 'reveal.js/plugin/highlight/highlight.esm';
import 'reveal.js/dist/reveal.css'
import 'reveal.js/dist/theme/black.css'
import 'reveal.js/plugin/highlight/zenburn.css'
//import '../css/dracula.css'

let deck = new Reveal({
   plugins: [ Markdown, Highlight ]
})
deck.initialize();